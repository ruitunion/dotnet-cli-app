using System.CommandLine;
using System.CommandLine.Binding;
using System.CommandLine.Builder;
using System.CommandLine.Help;
using System.CommandLine.Parsing;
using Spectre.Console;

namespace Cli;

public class CommandHandler
{
    public async Task Handle(params string[] args)
    {
        var left = new Option<double>(
            aliases: new[] { "-l", "--left" },
            description: "Left operand")
        {
            IsRequired = true,
            Arity = ArgumentArity.ExactlyOne
        };
        var right = new Option<double>(
            aliases: new[] { "-r", "--right" },
            description: "Right operand")
        {
            IsRequired = true,
            Arity = ArgumentArity.ExactlyOne
        };

        var sum = new Command("sum", "Sum of the two numbers");
        sum.Add(left);
        sum.Add(right);
        sum.SetHandler((left, right) => Console.WriteLine(left + right), left, right);

        var subtract = new Command("subtract", "Subtract one number from another");
        subtract.Add(left);
        subtract.Add(right);
        subtract.SetHandler(
            (sub) => Console.WriteLine(sub.Calc()),
            new SubtractBinder(left, right));

        var multiply = new Command("multiply", "Multiply one number by another");
        multiply.Add(left);
        multiply.Add(right);
        multiply.SetHandler((left, right, resultWriter) =>
        {
            var result = left * right;
            resultWriter.Write(result);
            Console.WriteLine(result);
        }, left, right, new ResultWriterBinder());

        var divide = new Command("divide", "Divide one number by another");
        divide.Add(left);
        divide.Add(right);
        divide.SetHandler((left, right, resultWriter) =>
        {
            var result = left / right;
            resultWriter.Write(result);
            Console.WriteLine(result);
        }, left, right, new ResultWriterBinder());

        var math = new Command("math", "Mathematical operations");
        math.Add(sum);
        math.Add(subtract);
        math.Add(multiply);
        math.Add(divide);
        math.SetHandler(() => Console.Write("Math command handler"));

        var matrix = new Command("matrix", "Fancy command that refers to Matrix movie");
        matrix.SetHandler(MatrixCommandHandler);

        var root = new RootCommand("CLI app example");
        root.Add(math);
        root.Add(matrix);

        var parser = new CommandLineBuilder(root)
            .UseDefaults()
            .UseHelp(ctx =>
                ctx.HelpBuilder.CustomizeLayout(x =>
                    HelpBuilder.Default
                        .GetLayout()
                        .Prepend(d =>
                            Console.WriteLine("This is a new section"))))
            .Build();

        await parser.InvokeAsync(args);
    }

    private async Task MatrixCommandHandler()
    {
        AnsiConsole.Clear();
        await AnsiConsole.Progress()
            .StartAsync(async ctx =>
            {
                var task = ctx.AddTask("[green]Searching...[/]");

                while (!ctx.IsFinished)
                {
                    await Task.Delay(150);
                    task.Increment(6.66);
                }
            });

        AnsiConsole.Clear();
        async Task WriteSequentially(string sentence)
        {
            var table = new Table()
                .MinimalBorder()
                .NoBorder()
                .LeftAligned();

            await AnsiConsole.Live(table).StartAsync(async ctx =>
            {
                foreach (var s in sentence)
                {
                    table.AddColumn($"[bold green]{s}[/]");
                    ctx.Refresh();
                    await Task.Delay(150);
                }
            });
        }

        await WriteSequentially("Wake up, Neo...");
        await Task.Delay(1500);
        AnsiConsole.Clear();

        await WriteSequentially("The Matrix has you...");
        await Task.Delay(1500);
        AnsiConsole.Clear();

        var width = Console.WindowWidth;
        var height = Console.WindowHeight;
        var columns = new int[width];

        while (true)
        {
            for (int x = 0; x < width; x++)
            {
                if (columns[x] == 0 || Random.Shared.Next(10) == 0)
                {
                    columns[x] = Random.Shared.Next(height);
                }

                AnsiConsole.Cursor.SetPosition(x, columns[x]);
                var c = (char)Random.Shared.Next(32, 127);
                if (c == '[' || c == ']') c = '*'; // [ and ] are special chars in Spectre.Console
                AnsiConsole.Markup($"[bold green]{c}[/]");
                columns[x]++;
            }

            await Task.Delay(75);
        }
    }
}

public record Subtract(double Left, double Right)
{
    public double Calc()
    {
        var result = Left - Right;
        File.WriteAllText("result.txt", $"{result}");
        return result;
    }
}

public class SubtractBinder : BinderBase<Subtract>
{
    private readonly Option<double> _left;
    private readonly Option<double> _right;

    public SubtractBinder(Option<double> left, Option<double> right) =>
        (_left, _right) = (left, right);

    protected override Subtract GetBoundValue(BindingContext bindingContext) =>
        new Subtract(
            bindingContext.ParseResult.GetValueForOption(_left),
            bindingContext.ParseResult.GetValueForOption(_right));
}

public class ResultWriter
{
    public void Write(double result) => File.WriteAllText("result.txt", $"{result}");
}

public class ResultWriterBinder : BinderBase<ResultWriter>
{
    private readonly ResultWriter _resultWriter = new ResultWriter();

    protected override ResultWriter GetBoundValue(BindingContext bindingContext) => _resultWriter;
}
